#include <iostream>
#include "TwoDimensionalShape.h"
#include "ShapeParameterException.h"

#ifndef __RECTANGLE_H__
#define __RECTANGLE_H__

class Rectangle : public TwoDimensionalShape {
private:
    length_unit m_length;
    length_unit m_width;
public:
    Rectangle(const length_unit length = 1, const length_unit width = 1) throw(ShapeParameterException);

    // getter and setter
    virtual length_unit getLength() const;

    virtual void setLength(const length_unit length) throw(ShapeParameterException);

    virtual length_unit getWidth() const;

    virtual void setWidth(const length_unit width) throw(ShapeParameterException);

    // override
    virtual length_unit getPerimeter() const override;

    virtual area_unit getArea() const override;

    virtual std::string getName() const override;

    virtual ~Rectangle() {}

    // friend function -- even though this is declared within the Rectangle class
    //                    declaration, it is NOT a member function of the Rectangle class
    /**
     * Provide the ability to read Rectangles from standard input stream.
     *
     * @param ins the input stream from which Rectangle attributes are extracted
     * @param target the Rectangle being "read" from the standard input stream
     * @return A reference to an istream object is returned that allows for cascading the stream extraction operator.
     */
    friend std::istream& operator >>(std::istream& ins, Rectangle& target);
};

// Nonmember Functions
/**
 * Overload the + operator to allow clients to create new Rectangles by adding together
 * two different Rectangles. The new rectangle has a width equal to the sum of the widths
 * and a length equal to the sum of the lengths.
 *
 * @param lhs the left hand operand in r1 + r2
 * @param rhs the right-hand operand in r1 + r2
 * @return A new Rectangle whose dimensions are the sum of the two operands dimensions
 */
Rectangle operator +(const Rectangle& lhs, const Rectangle& rhs);

/**
 * Overload the == operator that compares two Rectangles based upon having equal lengths and widths
 *
 * @param lhs the left-hand operand in r1 == r2
 * @param rhs the right-hand operand in r1 == r2
 * @return True is returned if and only if r1.length == r2.length and r1.width == r2.width.
 */
bool operator ==(const Rectangle& lhs, const Rectangle& rhs);

/**
 * Overload the != operator that compares two Rectangles for inequality.
 *
 * @param lhs the left-hand operand in r1 != r2
 * @param rhs the right-hand operand in r1 != r2
 * @return True is returned if and only if r1.length != r2.length or r1.width != r2.width.
 */
bool operator !=(const Rectangle& lhs, const Rectangle& rhs);

/**
 * Provide the ability to directly "print" Rectangles to standard output stream.
 *
 * @param outs the ostream we insert the given Rectangle's attributes
 * @param source the Rectangle whose attributes are inserted into the given ostream
 * @return A reference to an ostream object is returned that allows for cascading the stream insertion operator.
 */
std::ostream& operator <<(std::ostream& outs, const Rectangle& source);

#endif // __RECTANGLE_H__
